# README #

Pocket Duino用Bluetoothモジュール

CADはDesignsparkPCBです。パーツライブラリはこちらで公開しています。
[DesignSparkPCBオリジナルライブラリ](https://bitbucket.org/tarosay/designspark_originallibrary)

Copyright (C) 2014 Minao Yamamoto

License
-----------
 T_BT is free hardware distributed under the terms of the MIT license reproduced here. T_BT may be used for any purpose, including commercial purposes, at absolutely no cost. No paperwork, no royalties, no GNU-like "copyleft" restrictions, either. Just download it and use it.

T_BT is certified Open Source hardware. [osi certified] Its license is simple and liberal and is compatible with GPL. T_BT is not in the public domain and Minao Yamamoto keeps its copyright.

http://opensource.org/licenses/mit-license.php

(end of COPYRIGHT)